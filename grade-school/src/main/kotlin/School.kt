import java.util.*

class School {

    var db = mutableMapOf<Int, MutableList<String>>()

    fun db() : Map<Int, List<String>> = db

    fun add (name : String, grade : Int) {
        val names = db.getOrElse(grade) { mutableListOf<String>() }
        names.add(name)

        db.set(grade, names)
    }

    fun grade(grade : Int) : List<String> = db.getOrElse(grade) { listOf<String>() }
    fun sort() : Map<Int, List<String>> = db.toSortedMap().mapValues { it.value.sorted() }
}