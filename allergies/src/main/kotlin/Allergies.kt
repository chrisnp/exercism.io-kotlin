class Allergies(val score : Int) {

    fun isAllergicTo(allergen : Allergen) : Boolean = allergen.score and score != 0

    fun getList() : List<Allergen> {
        return  Allergen
                .values()
                .filter { isAllergicTo(it) }
    }

}