object Series {

    fun slices (size : Int, inputString : String) : List<List<Int>> {
        val digits = inputString.map { Integer.parseInt(it.toString()) }
        return (0..digits.size - size).map { digits.subList(it, it + size) }
    }
}