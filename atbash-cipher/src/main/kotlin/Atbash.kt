class Atbash {
    
    companion object {

        private val plain  = ('a'..'z').zip('z' downTo 'a').toMap()
        private val cipher = ('z' downTo 'a').zip('a'..'z').toMap()

        fun encode(text : String) : String = text
                                            .asSequence()
                                            .filter {it.isLetterOrDigit()}
                                            .joinToString("")
                                            .toLowerCase()
                                            .map {plain[it] ?: it}
                                            .joinToString("")
                                            .chunked(5) { it }
                                            .joinToString(" ")
                                                
        fun decode(text : String) : String = text
                                            .replace(" ", "")
                                            .map {cipher[it] ?: it}
                                            .joinToString("")
    }
}