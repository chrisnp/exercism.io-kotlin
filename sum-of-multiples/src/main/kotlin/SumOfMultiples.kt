class SumOfMultiples {

    companion object {

        fun sum (nums : Set<Int>, upTo : Int) : Int {
            return (0 until upTo).filter{nums.any{num -> it % num == 0}}.sum()
        }
    }
}