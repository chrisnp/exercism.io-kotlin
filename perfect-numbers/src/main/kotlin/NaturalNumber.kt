import Classification.*

enum class Classification {
    DEFICIENT, PERFECT, ABUNDANT
}


fun classify(naturalNumber: Int): Classification {

    if (naturalNumber <= 0) throw RuntimeException("$naturalNumber is not a natural number")

    val aliquotSum : Int = (1..naturalNumber/2).filter { naturalNumber % it == 0 }.sum()

    return when {
        aliquotSum > naturalNumber  -> Classification.ABUNDANT
        aliquotSum == naturalNumber -> Classification.PERFECT
        else                        -> Classification.DEFICIENT
    }
}
