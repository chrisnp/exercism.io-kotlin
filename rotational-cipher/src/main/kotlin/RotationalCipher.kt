class RotationalCipher(private val offset : Int) {

    fun encode(text : String) = 
        text.fold("") {
            acc, c -> acc + rotate(c)
        }
    
    private fun rotate(c : Char) : Char {
        return when {
            c.isLowerCase() -> ((c.toInt() - 97 + offset) % 26 + 97).toChar()
            c.isUpperCase() -> ((c.toInt() - 65 + offset) % 26 + 65).toChar()
            else            -> c
        }
    }
}