import Signal.*

object HandshakeCalculator {

    fun calculateHandshake (num : Int) : List<Signal> {

        val bin = Integer.toBinaryString(num)

        return bin
              .reversed()
              .toList()
              .zip(listOf(WINK, DOUBLE_BLINK, CLOSE_YOUR_EYES, JUMP))
              .fold(emptyList<Signal>()) { acc : List<Signal>, handShake : Pair<Char, Signal> ->
                    if (handShake.first == '1') acc.plus(handShake.second) else acc 
                }
              .let { result -> result.takeIf { num < 16 } ?: result.reversed() }
        }
}