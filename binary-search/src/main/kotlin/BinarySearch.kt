class BinarySearch {

    companion object {

        val NOT_FOUND : Int = -1

        fun <T: Comparable<T>> List<T>.isSorted() : Boolean = 
            (0 until lastIndex).all { i -> this[i] <= this[i + 1] }

        fun <T: Comparable<T>> search(list: List<T>, value: T ): Int {
            require(list.isSorted())
            if (list.isEmpty())
                return NOT_FOUND
            return binsearch(list, value, 0)
        }

        private tailrec fun <T: Comparable<T>> binsearch (list: List<T>, value: T, offset : Int) : Int {
            if (list.size == 1) {
                return if (list.first() == value) offset else NOT_FOUND 
            }
            val midx = list.size / 2
            val mid = list[midx]
            if(value < mid) {
                return binsearch(list.subList(0, midx), value, offset)            
            }
            return binsearch(list.subList(midx, list.size), value, midx + offset)
        }
    }
}

