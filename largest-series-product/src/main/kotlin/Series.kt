import java.lang.Character.isDigit

class Series (private val series : String) {

    init {
        require (series.all(::isDigit))
    }

    fun getLargestProduct (span : Int) :Int {

        require(span >= 0 && span <= series.length) 

        return if (span == 0) 1 
               else (0..series.length - span)
               .map { i -> series.slice (i..i + span - 1)
                          .map { it.toString().toInt()}
                          .reduce { d1, d2 -> d1 * d2 }
               }.max() ?: 1
    }

}