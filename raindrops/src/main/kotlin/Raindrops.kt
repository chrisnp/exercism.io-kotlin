class Raindrops {
    companion object {

        fun convert (num : Int) : String {
            var drops = ""
            if (num % 3 == 0) drops += "Pling"
            if (num % 5 == 0) drops += "Plang"
            if (num % 7 == 0) drops += "Plong"
            if (drops.isEmpty()) drops += num.toString()
            return drops 
        }
     }
}