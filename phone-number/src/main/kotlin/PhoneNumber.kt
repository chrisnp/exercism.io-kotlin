class PhoneNumber(val phoneNumber : String) {
 
    var number : String? = phoneNumber
                          .filter { it.isDigit() } 
                          .let {
                              when (it.length) {
                                  11   -> if (it[0] == '1') it.drop(1) else null
                                  10   -> it
                                  else -> null
                              }
                          }?.takeIf { (it[0] in '2'..'9') && (it[3] in '2'..'9') }

    val areaCode: String?    = number?.take(3)
    val xchange : String?    = number?.drop(3)?.take(3)
    val subscriber : String? = number?.takeLast(4)


    override fun toString(): String = "(${number?.take(3)}) ${number?.drop(3)?.take(3)}-${number?.takeLast(4)}"

}