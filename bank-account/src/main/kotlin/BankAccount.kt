class BankAccount {

    private val lock = Any()

    var balanceAccount : Long = 0
        private set

    var closed = false
        private set

    val balance : Long
        get() {
            synchronized(lock) {
                if (closed) throw IllegalStateException()
                return balanceAccount
            }  
        }

    fun close() {
        synchronized(lock) {
            closed = true
        }
    }

    fun adjustBalance(amount : Long) {
        synchronized(lock) {
            if (closed) throw IllegalStateException()
            balanceAccount = balance + amount
        }
    }

}