class ScrabbleScore {

    companion object {

        fun scoreWord (word : String) : Int {
            return word.fold(0, {score, ch -> score + when(ch.toUpperCase()) {
                                                'Q', 'Z'                -> 10
                                                'J', 'X'                -> 8
                                                'K'                     -> 5
                                                'F', 'H', 'V', 'W', 'Y' -> 4
                                                'B', 'C', 'M', 'P'      -> 3
                                                'D', 'G'                -> 2
                                                'A', 'E', 'I', 'O', 'U' -> 1
                                                'L', 'N', 'R', 'S', 'T' -> 1
                                                else -> throw IllegalArgumentException("Not a scrabble letter")
                                            }
            })
        }
    }
}