class Sieve {

    companion object {

        fun primesUpTo (num : Int) : List<Int> {
            
            if (num <= 1 ) return emptyList()
            if (num == 2 ) return listOf(2)
 
            val isPrime = Array(num, { true })
            var primes  = ArrayList<Int>()

            (2..num - 1).forEach { n -> 
                if (isPrime[n]) {
                    primes.add(n)
                    (n..num - 1 step n).forEach { isPrime[it] = false }
                    }
                }
            return primes
        }
    }
}