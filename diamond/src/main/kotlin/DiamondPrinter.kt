class DiamondPrinter {

    private val CHARS = ('A'..'Z')

    fun printToList(char : Char) : List<String> {

        if (char == 'A') return listOf("A")

        val charIdx = CHARS.indexOf(char)
        val rowLen  = (2 * charIdx) + 1

        val top : List<String> = ('A' until char).foldIndexed(emptyList()) { i, acc, ch ->
            val outside = " ".repeat(charIdx - i)
            
            if ( i == 0 ) {
                acc + (outside + ch + outside)
            } else {
                val inside = " ".repeat(rowLen - (2 * (charIdx - i)) - 2 ) 
                acc + (outside + ch + inside + ch + outside)
            }
        } 
        val bottom = top.reversed()
        val midLine = char + " ".repeat(rowLen - 2) + char
        return top + midLine + bottom        
    }
}
