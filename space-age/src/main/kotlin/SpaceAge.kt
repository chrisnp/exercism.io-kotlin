import Planet.*

const val EARTH_ORBITAL = 60 * 60 * 24 * 365.25 

enum class Planet (val sec : Double) {
    Mercury (0.2408467 * EARTH_ORBITAL),
    Venus (0.61519726 * EARTH_ORBITAL),
    Earth (1.0 * EARTH_ORBITAL),
    Mars (1.8808158 * EARTH_ORBITAL),
    Jupiter (11.862615 * EARTH_ORBITAL),
    Saturn (29.447498 * EARTH_ORBITAL),
    Uranus (84.016846 * EARTH_ORBITAL),
    Neptune (164.79132 * EARTH_ORBITAL);
    fun earthYears (sec : Long) = Math.round((sec / this.sec ) * 100.0) / 100.0
    
}

class SpaceAge constructor(val seconds : Long) {
    
    fun onMercury() = Mercury.earthYears(seconds)
    fun onVenus()   = Venus.earthYears(seconds)
    fun onEarth()   = Earth.earthYears(seconds)
    fun onMars()    = Mars.earthYears(seconds)
    fun onJupiter() = Jupiter.earthYears(seconds)
    fun onSaturn()  = Saturn.earthYears(seconds)
    fun onUranus()  = Uranus.earthYears(seconds)
    fun onNeptune() = Neptune.earthYears(seconds)

}