class CollatzCalculator {
    companion object {

        tailrec fun computeStepCount (num : Int, counter : Int = 0) : Int {
            require(num > 0) {"Only natural numbers are allowed"}

            return when {
                    num == 1     -> counter
                    num % 2 == 0 -> computeStepCount(num / 2, counter + 1)
                    num % 2 == 1 -> computeStepCount(3 * num + 1, counter + 1)
                    else -> throw IllegalArgumentException()
            }
        }
    }
}