class SpiralMatrix {
    companion object {

        fun ofSize(size : Int) : Array<Array<Int>> {
            var spiral = Array(size) { Array(size) {0}}
            if (size > 0) {
                var y = 0
                var x = 0
                var xmin = 0
                var ymin = 1
                var xmax = size - 1 
                var ymax = size - 1
                var dir = 0
                var cnt = 1
                while ( spiral[y][x] == 0 ) {
                    spiral[y][x] = cnt++
                    if (size > 1) {
                        when (dir) {
                            0 -> if (x < xmax) x++ else { dir++; y++; xmax-- }
                            1 -> if (y < ymax) y++ else { dir++; x--; ymax-- }
                            2 -> if (x > xmin) x-- else { dir++; y--; xmin++ }
                            3 -> if (y > ymin) y-- else { dir = 0; x++; ymin ++}
                        }
                    }
                }
            }
            return spiral
        }
    }
}