object PascalsTriangle {
    fun computeTriangle(rows : Int) : List<List<Int>> {

        require(rows >= 0) {"Rows can't be negative!"}

        return (0 until rows).fold(mutableListOf()) { 
                triangle, row -> triangle.add(
                (0 until row).fold(listOf(1)) {
                    acc, i -> acc + (acc[i] * (row - i) / (i + 1))  
                })
                triangle
        }
    }
}