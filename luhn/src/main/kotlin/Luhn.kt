class Luhn {

    companion object {

        private fun String.noInvalidChars() : Boolean {
            return when {
                this.contains(Regex("[a-zA-Z]|[,.!?\\-@£$%^&*()_+={}<>]")) -> false
                else                                                       -> true
            }
        }

        fun isValid(num : String) : Boolean {

            val cleanNum = num.replace(" ", "")
            if ( !cleanNum.noInvalidChars() ) return false

            val sumLuhn = num.filter{it.isDigit()}
                         .map{ Integer.parseInt(it.toString())}
                         .asReversed()
                         .mapIndexed { idx, dig ->
                            if (idx % 2 == 1) { if (2 * dig > 9) ((2 * dig) - 9) else (2 * dig) } else dig }
                         .sum()
            return ( sumLuhn == 0 && num.count {it == '0'} > 1) 
                || ( sumLuhn != 0 && sumLuhn % 10 == 0 )
        } 
    }
}
