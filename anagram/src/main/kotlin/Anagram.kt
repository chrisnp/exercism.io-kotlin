class Anagram (val word : String) {

    fun match (candidates : List<String>) : Set<String> {
        return candidates
               .filterNot { it.toLowerCase() == word.toLowerCase() }
               .filter {it.toLowerCase().toList().sorted() == word.toLowerCase().toList().sorted() }
               .toSet() 
    }
}