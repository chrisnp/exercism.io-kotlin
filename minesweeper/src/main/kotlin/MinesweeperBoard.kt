
private const val MINE = '*'
private const val SPACE = ' ' 

data class MinesweeperBoard(val squares: List<String>) {

    private val rows by lazy {
        squares.size
    }

    private val columns by lazy {
        if (squares.isEmpty()) 0 else squares[0].length
    } 

    fun withNumbers () = (0 until rows).map { rowWithNumbers(it) }

    private fun rowWithNumbers (row : Int) = (0 until columns)
                                            .map { column -> mineCount(row, column) }
                                            .joinToString("")

    private fun mineCount (row : Int, col : Int) : Char {
        if (squares[row][col] == MINE) { 
            return MINE
        }
        val count = adjacentMines(row, col)
        return if (count > 0) Character.forDigit(count, 10) else SPACE
    }

    private fun adjacentMines (row : Int, col : Int) : Int {
        var numMines = 0
        val minRow = Math.max(row - 1, 0)
        val maxRow = Math.min(row + 1, rows - 1)
        val minCol = Math.max(col - 1, 0)
        val maxCol = Math.max(col + 1, columns - 1)
        for (currRow in minRow..maxRow) {
            for (currCol in minCol..maxCol) {
                if (squares[currRow][currCol] == MINE) {
                    numMines += 1
                }
            }
        }
        return numMines
    }
}