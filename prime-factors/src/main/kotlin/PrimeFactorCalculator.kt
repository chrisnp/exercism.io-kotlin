class PrimeFactorCalculator {

    companion object {

        fun primeFactors(num : Long) : List<Long> {

            if (num < 2) return emptyList()

            val primeFactors = mutableListOf<Long>()
            var rem = num
            var curr = 2L
            while ( curr <= rem / curr ) {
                while ( rem % curr == 0L ) {
                    primeFactors.add(curr)
                    rem /= curr
                }
                curr++
            }
            if ( rem > 1 ) primeFactors.add(rem)
            return primeFactors
        } 

        fun primeFactors(num : Int): List<Int> = primeFactors(num.toLong()).map { it.toInt() }
    }
}