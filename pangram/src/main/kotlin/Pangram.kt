class Pangram {

    companion object {

        fun isPangram (str : String) : Boolean {

            return str.toLowerCase().toList().containsAll(('a'..'z').toList())
            
        }
    }
}