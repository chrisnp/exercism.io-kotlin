class Acronym {

    companion object {

        private fun String.unCamelCase() = this.replace(Regex("(?<=[a-z])(?=[A-Z][a-z])"), " ")    

        fun generate (phrase : String) : String {
            return phrase
                    .unCamelCase()
                    .split(" ", "-")
                    .fold("") { acro, word -> acro + word.first().toUpperCase() }
        }
    }
}