class Dna (strand : String ) {

    init {
        require (strand.all { it in "ACGT" }) {"invalid nuclotide(s) in sequence"}
    }

    val nucleotideCounts: Map<Char, Int> = "ACGT".map { it to 0 }.toMap() + strand.groupingBy { it }.eachCount()
}