class Isogram {
    companion object {
        fun isIsogram (input : String) : Boolean {
            val normal = input
                        .toLowerCase()
                        .filter { it.isLetter() }
            return normal.toCharArray().distinct().size == normal.length
        }
    }
}