import java.time.LocalDate
import java.time.LocalTime
import java.time.LocalDateTime


class Gigasecond(startTime : LocalDateTime) {
    
    private val GIGASECOND = Math.pow(10.0, 9.0).toLong()  
    constructor(startTime: LocalDate): this(startTime.atTime(LocalTime.MIDNIGHT))
    val date = startTime.plusSeconds(GIGASECOND)

}
