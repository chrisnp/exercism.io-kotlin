object BracketPush {

    val brackets = "[]{}()".toSet()

    fun replaceMatching(input : String) = input.replace("[]", "").replace("{}", "").replace("()", "")

    fun isValid(input : String) : Boolean {
        tailrec fun loop(remn : String) : Boolean {
            if (remn.isEmpty()) {
                return true
            } else {
                val upd = replaceMatching(remn) 
                return if (upd.length == remn.length) false else loop(upd)
            }
        }
        return loop(input.filter { it in brackets })
    }
}

