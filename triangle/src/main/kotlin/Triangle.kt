class Triangle (val sideA : Double, val sideB : Double, val sideC : Double) {
    constructor(sideA : Int, sideB : Int, sideC : Int) : 
        this(sideA.toDouble(), sideB.toDouble(), sideC.toDouble())
    init {
        val inequality   = (sideA + sideB > sideC) && (sideA + sideC > sideB) && (sideB + sideC > sideA)
        val notZeroSides = listOf(sideA, sideB, sideC).map { it != 0.0 }.all { it }
        require(inequality) { "Triangle Inequality does not hold" }
        require(notZeroSides) { "No side can be of length 0" }
    }
    val distinctLengths = listOf(sideA, sideB, sideC).toSet().size
    val isEquilateral : Boolean = distinctLengths == 1
    val isIsosceles   : Boolean = distinctLengths <= 2
    val isScalene     : Boolean = distinctLengths == 3
}