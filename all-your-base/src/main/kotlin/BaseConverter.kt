class BaseConverter(base : Int, digits : IntArray) {

    private val numeral : Int

    init {
        require(base >= 2) { "Bases must be at least 2." }
        require(digits.isNotEmpty()) { "You must supply at least one digit." }
        require(digits.size == 1 || digits[0] != 0) { "Digits may not contain leading zeros." }
        require(digits.min()!! >= 0)   { "Digits may not be negative." }
        require(digits.max()!! < base) { "All digits must be strictly less than the base." }

        numeral = digits.fold(0) { acc, x -> acc * base + x }    
    }

    fun convertToBase (newBase : Int) : IntArray {
        require(newBase >= 2) { "Bases must be at least 2." }
        return when (numeral) {
            0 -> intArrayOf(0)
            else -> {
                val newDigits = mutableListOf<Int>()
                var rem  = numeral
                var mult = 1
                while (rem > 0) {
                    mult *= newBase
                    newDigits.add((rem % mult) / (mult / newBase))
                    rem -= rem % mult
                }
                return newDigits.reversed().toIntArray()
            }
        }
    } 
}