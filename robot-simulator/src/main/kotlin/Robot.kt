import Orientation.*


class Robot(var gridPosition : GridPosition = GridPosition(0, 0), var orientation : Orientation = NORTH) {

    fun turnLeft() {
        orientation = when (orientation) {
            NORTH -> WEST
            WEST -> SOUTH
            SOUTH -> EAST
            EAST -> NORTH
        }
    }

    fun turnRight() {
        orientation = when (orientation) {
            NORTH -> EAST
            EAST -> SOUTH
            SOUTH -> WEST
            WEST -> NORTH
        }
    }

    fun advance() {
        gridPosition = when (orientation) {
            NORTH -> GridPosition(gridPosition.x,     gridPosition.y + 1)
            EAST -> GridPosition(gridPosition.x + 1,     gridPosition.y)
            SOUTH -> GridPosition(gridPosition.x,     gridPosition.y - 1)
            WEST -> GridPosition(gridPosition.x - 1,     gridPosition.y)
        }
    }

    fun simulate (commands : String) {
        commands.forEach { command ->
            when (command) {
                'L' -> turnLeft()
                'R' -> turnRight()
                'A' -> advance()
            }
        }
    }
}