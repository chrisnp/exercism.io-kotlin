class Squares constructor (val value :Int) {

    fun sumOfSquares () : Int {
        return (0..value).reduce { x, y -> x + y * y }
    }

    fun squareOfSum () : Int {
        val sum = (0..value).reduce { x, y -> x + y } 
        return sum * sum
    }

    fun difference () : Int {
        return squareOfSum() - sumOfSquares()
    }
}