object ETL {

    fun transform(old : Map<Int, List<Char>>) : Map<Char, Int> {
        return old
              .flatMap { it.value.map { letter -> Pair(letter.toLowerCase(), it.key) } }
              .toMap()
    }
}