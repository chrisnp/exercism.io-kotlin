import java.util.*

class Robot {
    
    var name : String

    init {
        name = generateName()
    }

    fun reset() {
        name = generateName()
    }

    fun generateName() : String {
        val random = Random()
        return listOf(random.letter(),
                      random.letter(), 
                      random.digit(), 
                      random.digit(), 
                      random.digit())
                     .joinToString("")
    }

    private fun Random.letter(): Char = (nextInt(26) + 'A'.toInt()).toChar()
    private fun Random.digit(): Char = (nextInt(10) + '0'.toInt()).toChar()
}