object Prime {

	fun nth(n : Int) : Int {

		require (n>0) { "There is no zeroth prime."}
		return ArrayList<Int>().apply { repeat(n) { addNextPrime() } }.last()

	}
	
	fun MutableList<Int>.addNextPrime() {

		if (isEmpty()) { add(2); return }
		
		var next = last() + 1
		while ( any { next % it == 0} ) { next++ }
		add(next)
	}
}
