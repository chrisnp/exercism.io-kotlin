class Matrix (val matrix : List<List<Int>>) {

    val saddlePoints =  matrix
                       .foldIndexed (setOf<MatrixCoordinate>()) { 
                           ridx, acc, row -> acc + (row.foldIndexed (setOf<MatrixCoordinate>(), 
                                                      accumulateSaddlePoints(ridx, row))) 
                        }

    private fun accumulateSaddlePoints(ridx : Int, row : List<Int>) : (Int, Set<MatrixCoordinate>, Int) -> 
                Set<MatrixCoordinate> = { cidx, acc, value -> 
                                            if (isSaddlePoint(value, row, cidx)) 
                                                acc + MatrixCoordinate(ridx, cidx) 
                                            else 
                                                acc 
                                        }

    private fun isSaddlePoint(value : Int, row : List<Int>, col : Int) = value == row.max()!! 
                                                                      && value == matrix.map { it[col] }.min()!!

}