object Bob {

    fun hey(words : String) : String {
        val input = words.trim()
        return when {
            input.shout()    -> "Whoa, chill out!"
            input.question() -> "Sure."
            input.silent()   -> "Fine. Be that way!"
            else             -> "Whatever."    
        }

    }

    private fun String.shout() : Boolean = this.contains(Regex("[A-Z]")) && this == this.toUpperCase()
    private fun String.question() : Boolean = this.endsWith("?")
    private fun String.silent() : Boolean = this.isBlank()

}