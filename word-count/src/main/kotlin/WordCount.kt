class WordCount {
    companion object {
        fun phrase(input: String): Map<String, Int> {
            return input
                  .toLowerCase()
               //   .filter { Character.isLetterOrDigit(it) || it == '\'' }
                  .split("\'?[^a-z0-9\']+\'?".toRegex())
                  .filter( String::isNotEmpty ) 
                  .groupBy { it }
                  .map { it.key to it.value.size }
                  .toMap()
        }
    }
}